# Zoomsense Plugin - Chat Submission

This plugin allows meeting hosts to define a list of "submissions" to accept. Participants in the meeting can then make submissions via the chat.

This is best understood by an example:

If this plugin is configured to accept submissions called "Favourite car", participants may then "submit" their favourite car by sending
chat messages of the form "car: Ferrari".

# Types

This package also exposes a `types` package called "@zoomsense/chat-submission-types" that defines the shape of the types of this packages data.

### Deployment

From the root of this repository:

`npm publish --access public --workspace types`
