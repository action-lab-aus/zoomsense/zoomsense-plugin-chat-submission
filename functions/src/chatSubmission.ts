import * as functions from 'firebase-functions';
import { parseResponse } from './parseResponse';
import { db } from './db';
import { typedFirebaseFunctionRef } from '@zoomsense/zoomsense-firebase';

// Whenever a new chat message is detected, check if it is a response to
// one of the configured submissions
export const handleNewChat = typedFirebaseFunctionRef(functions.database.ref)(
  '/data/chats/{meetingId}/{sensor}/{chatId}',
).onCreate(async (snapshot, context) => {
  const { meetingId, sensor, chatId } = context.params as { meetingId: string; sensor: string; chatId: string };

  // Messages with ID 'message' are instructions to the bot, not actual chat messages
  if (chatId === 'message') return;

  const config = (await db.ref(`/config/${meetingId}/current/currentState/plugins/chatsubmission`).get()).val();

  // Check if plugin is enabled
  if (!config || !config.enabled) return;

  // Fetch chat submission settings
  const { submissions } = config;
  if (!submissions) return;

  // Get the message
  const { msg: messageContent, msgSender, msgSenderName, timestamp } = snapshot.val()!;

  // Check if chat starts with any of the prefixes defined in the config
  const matches = submissions
    .map(el => ({ response: parseResponse(messageContent, el), ...el }))
    .filter(({ response }) => response);

  const firstMatch = matches[0];

  // If this message didn't match any submissions in the config
  if (!firstMatch) return;

  const { response, name, prefix } = firstMatch;

  if (!response) return;

  const currentSection = (await db.ref(`/config/${meetingId}/current/currentSection`).get()).val();

  if (currentSection === null) return;

  await db.ref(`data/plugins/chatSubmission/${meetingId}`).push({
    section: currentSection,
    timestamp,
    sensor,
    senderId: msgSender,
    senderName: msgSenderName,
    submissionPrefix: prefix!,
    submissionName: name!,
    submission: response,
  });

  // Acknowledge the chat submission with a private message (sent by the receiving sensor).
  await db.ref(`data/chats/${meetingId}/${sensor}/message`).push({
    msg: `Received your submission: ${response}`,
    receiver: msgSender
  });
});
